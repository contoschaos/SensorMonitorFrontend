# SensorMonitor

## Development server
Run `npm install`
Run `ng serve`
Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build
Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Description
This project is meant as frontend to `https://gitlab.com/contoschaos/SensorMonitorBackend`.
