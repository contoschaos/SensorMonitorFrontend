import { Component,  OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RestService } from '../rest.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit {
  public ip : string | undefined;

  constructor(private outRoute : Router,private restService: RestService) {
  }

  ngOnInit(): void {
    this.ip = this.restService.getEndpoint();
  }

  /**
   * Sets ip address as an endpoint in rest service.
   * @returns 
   */
  public set() : void{
    if(!this.ip || !this.isIP()){
      return;
    }
    this.restService.setEndpoint(this.ip)
    this.outRoute.navigate(['/']) 
  }

  /**
   * Uses regex to checks if string it is valid ip.
   * @returns 
   */
  isIP() : boolean{
    if(this.ip)
      return /^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(this.ip)//TODO
    else
      return false;
  }
}
