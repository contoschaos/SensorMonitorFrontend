import { Injectable, Optional, SkipSelf } from '@angular/core';
import { firstValueFrom } from 'rxjs';
import { RestService } from './rest.service';

@Injectable({
  providedIn: 'root'
})
export class DataDictionaryService {
  private dictionary: any = undefined;
  private reverse: any = undefined;

  constructor(private rest: RestService, @Optional() @SkipSelf() private self?: DataDictionaryService) {
    if (self) {
      throw new Error("Service: InternalUpdateService is not singleton.")
    }
  }

  translate(id: string) {
    let result = this.dictionary.get(id)
    return result ? result : id;
  }

  translateReverse(id: string) {
    let result = this.reverse.get(id)
    return result ? result : id;
  }

  loadDictionaryIfNecessary() {
    if (!this.dictionary || !this.reverse) {
      this.rest.getDictionary().subscribe((dictionary) => {
        let object = JSON.parse(dictionary);
        let wholeArray = object.sensors.concat(object.attributes);
        this.dictionary = new Map(wholeArray.map(this.mapFunction));
        this.reverse = new Map(wholeArray.map(this.mapReverseFunction));
      });
    }
  }

  async loadDictionaryAwait() {
    let dictionary = await firstValueFrom(this.rest.getDictionary());

    let object = JSON.parse(dictionary);
    let wholeArray = object.sensors.concat(object.attributes);

    this.dictionary = new Map(wholeArray.map(this.mapFunction));
    this.reverse = new Map(wholeArray.map(this.mapReverseFunction))
  }

  private mapFunction(word: any) {
    let keys = Object.keys(word)
    return [keys[0], word[keys[0]]]
  }

  private mapReverseFunction(word: any) {
    let keys = Object.keys(word)
    return [word[keys[0]], keys[0]]
  }
}
