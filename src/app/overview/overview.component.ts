import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { interval, Subscription } from 'rxjs';
import { DataDictionaryService } from '../data-dictionary.service';
import { InternalUpdateService } from '../internal-update.service';
import { RestService } from '../rest.service';

const REFRESH_TIME = 60000; //milliseconds

/**
 * Expected parsed data structure.
 */
type ParsedData = {
  data: any
}

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.sass']
})
export class OverviewComponent implements OnInit, OnDestroy {
  public ip: string | undefined;
  public refreshing: boolean = false;
  private subRefresh: Subscription | undefined;

  /** All shown sensors */
  sensorsNames: Set<string> = new Set;

  @ViewChild('cam') video: ElementRef<HTMLMediaElement> | undefined;

  constructor(private route: ActivatedRoute,// HAVE TO BE HERE FOR HTML ROUTING
    private restService: RestService,//
    private internalUpdate: InternalUpdateService,//
    private dataDictionaryService: DataDictionaryService) { }

  /**
   * Initialize translator and start data refresher.
   */
  ngOnInit(): void {
    this.dataDictionaryService.loadDictionaryIfNecessary();
    this.refreshData(this.restService);
    this.subRefresh = interval(REFRESH_TIME).subscribe((number) => {
      this.refreshData(this.restService);
    });
    this.ip = this.restService.getEndpoint()

    /** Reloads video if necessary */
    setTimeout(() => {
      if (this.video && this.video.nativeElement.readyState !== 4) {
        this.video.nativeElement.load()
        this.video.nativeElement.play()
      }
    }, 200)
  }

  /**
   * Clean up.
   */
  ngOnDestroy(): void {
    if (this.subRefresh) {
      this.subRefresh.unsubscribe();
    }
  }

  /**
   * Refreshes data.
   */
  refresh(): void {
    this.refreshData(this.restService);
  }

  /**
   * Try update all sensor data.
   * @param pRestService 
   */
  private refreshData(pRestService: RestService): void {
    this.refreshing = true;
    pRestService.getCurrent().subscribe(dataRaw => {
      const dataParsed: ParsedData | undefined = this.parseJSON(dataRaw);

      if (!dataParsed || !dataParsed.data) {
        console.error("No data!")
        return;
      }

      dataParsed.data.forEach((sensor: any) => {
        let keys = Object.keys(sensor)
        //Update sensor
        let initialName = keys[0];
        //Translating here, because if done in service,
        //there will be additional loop to translate, before sending it here  
        let sensorName = this.dataDictionaryService.translate(initialName);

        let hasBeenCreated = this.sensorsNames.has(sensorName);
        if (!hasBeenCreated) {
          this.sensorsNames.add(sensorName)
        }

        //Update with data
        let copy: any = {}
        copy[sensorName] = sensor[initialName];
        if (keys.length > 1) {
          copy['lvl'] = sensor['lvl']
          copy['msg'] = sensor['msg']
        }
        this.internalUpdate.pushSensorData(copy);
      });

      this.refreshing = false;
    })
  }

  /**
   * Try parse JSON.
   * @param dataRaw 
   * @returns 
   */
  private parseJSON(dataRaw: any): ParsedData | undefined {
    try {
      return JSON.parse(dataRaw)
    } catch (err) {
      console.error("Failed to parse JSON during refresh job!")
      return undefined;
    }
  }
}
