import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EChartsOption } from 'echarts';
import { DataDictionaryService } from '../data-dictionary.service';
import { RestService } from '../rest.service';

/**
 * Expected parsed data structure.
 */
type ParsedData = {
  data: any
}

/**
 * One datapint structure.
 */
type Datapoint = {
  name?: string;       // Datapoint name
  value: [Date, any][];  // [x axis value, y axis value]
}

/**
 * Need to have keys as defined in Ngx-charts.
 */
type UPDATE_OPTIONS = {
  series: any[],       // Data in series
  legend: { data: any[] } // top used for visual margin
}

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.sass']
})
export class HistoryComponent implements OnInit {
  /** Name of a sensor. */
  @Input()
  sensorName: string = "";

  @ViewChild('dateInput', {static:true}) 
  dateInput!: ElementRef;

  /** Date of dataset */
  dataDate: Date = new Date();

  /** Date Limit */
  maxDate = new Date().toISOString().split('T')[0];

  /** Ngx-Charts params */
  options: any;                                           // Static otions
  updateOptions: UPDATE_OPTIONS = this.getEmptyUpdate(); // Dynamic options

  /**
   * Storing data for graph. 
   * When updating graph, it is copied to its series.
   */
  private allData: Map<string, Datapoint[]> = new Map<string, Datapoint[]>();

  constructor(private dataDictionaryService: DataDictionaryService,//
    private route: ActivatedRoute,//
    private restService: RestService) { }

  ngOnInit(): void {
    this.route.queryParams.subscribe(async params => {
      this.sensorName = params['sensorname']

      // Initialize chart options:
      await this.loadDictionary();
      this.options = this.getOptions();

      //Init date and data
      this.setDateProcess(new Date());
    });
  }

  /**
   * Sets a chosen date.
   * @param event 
   */
  setDate(event: any): void {
    let date = event.target.valueAsDate;

    if (event.target.valueAsDate) {
      this.setDateProcess(date);
    }
  }

  /**
   * Reset data to empty state.
   */
  resetData(): void {
    this.allData = new Map<string, Datapoint[]>()
    this.updateOptions = this.getEmptyUpdate();
  }

  /**
   * By checking update options on NG level it triggers refresh 
   * (Necessary for init without route)
   * @returns availability of update data
   */
  isDynamicDataLoaded(): boolean {
    return this.updateOptions.series.length > 0;
  }

  /**
   * Process data for given time stamp.
   * (There are multiple time stamps with same date! (x axis))
   * @param timeStamp 
   * @param data 
   */
  private processTimeStamp(timeStamp: string, data: ParsedData): void {
    let time = timeStamp.split("-");
    let sensorData = data.data[timeStamp][this.sensorName];

    if (!sensorData) {
      sensorData = data.data[timeStamp][this.dataDictionaryService.translate(this.sensorName)];
    }

    if (sensorData) {
      let atributes = Object.keys(sensorData);

      let updatedDate = new Date(this.dataDate);
      updatedDate.setHours(Number(time[0]), Number(time[1]));

      atributes.forEach(att => {
        this.processAttributes(updatedDate, att, sensorData[att]);
      });
      //this.data.shift();
    }
  }

  /**
   * Formats data for given Date, Attribute to be displayed in graph.
   * Process each attribute from timestamp.
   * @param date 
   * @param attName 
   * @param value 
   */
  private processAttributes(date: Date, attName: string, value: any): void {
    let formatedData: Datapoint = {
      value: [date, value]
    };

    let dataList = this.allData.get(attName);
    if (dataList) {
      dataList.push(formatedData);
    } else {
      this.allData.set(attName, [formatedData])
    }

  }

  /**
   * Cheks if data are provided and starts process timestamps for all data.
   * 
   * @param data
   * @returns 
   */
  private setData(data: ParsedData): void {
    if (!this.dataDate || !this.sensorName || !data.data) {
      console.error("No data to set!")
      return;
    }

    let timeStamps: string[] = Object.keys(data.data);
    timeStamps.forEach(timeStamp => {
      this.processTimeStamp(timeStamp, data)
    });
  }

  /**
   * By setting a date, a request for datset is send and further processed. 
   * Resulting in refresh of the graph.
   * @param date 
   */
  private async setDateProcess(date: Date) {
    this.dataDate = date;
    this.restService.getHistory(date).subscribe(dataRaw => {
      let dataParsed: ParsedData;
      try {
        dataParsed = JSON.parse(dataRaw)
        this.resetData();
        this.setData(dataParsed);
        this.updateGraph();
      } catch (err) {
        console.error(err)
        return;
      }
    });
  }

  private createSeries(name: string, data: Datapoint[]) {
    return {
      name: this.dataDictionaryService.translate(name),
      seriesName: this.dataDictionaryService.translate(name),
      type: 'line',
      showSymbol: false,
      hoverAnimation: false,
      data: data
    }
  }

  private getEmptyUpdate(): UPDATE_OPTIONS {
    return {
      series: [],
      legend: { data: [] }
    }
  }

  private async loadDictionary() {
    return await this.dataDictionaryService.loadDictionaryAwait();
  }

  private addZero(timeNumber: number): string | number {
    if (timeNumber < 10) {
      return '0' + timeNumber;
    }
    return timeNumber;
  }

  private getOptions(): EChartsOption {
    return {
      title: {
        text: this.dataDictionaryService.translate(this.sensorName),
      }, grid: {
        top: "30%",
      },
      tooltip: {
        trigger: 'axis',
        formatter: (params: any) => {
          //Date is same for all attribute params (it groups them)
          let stringBuild = [this.addZero(params[0].value[0].getHours()),  //
          this.addZero(params[0].value[0].getMinutes())] //
            .join(':').concat('<br>')

          //HTML format for tooltip
          params.forEach((serie: any) => {
            stringBuild = stringBuild.concat(serie.seriesName, '=', serie.value[1], '<br>')
          });

          return stringBuild;
        },
        axisPointer: {
          animation: false
        }
      },
      xAxis: {
        type: 'time',
        splitLine: {
          show: true
        }
      },
      yAxis: {
        type: 'value',
        //tickPositioner:()=>{return [1,10,100,1000]},
        boundaryGap: [0, '100%'],
        splitLine: {
          show: true
        },
        scale: true
      }, dataZoom: [
        {},
        {
          type: "inside"
        }
      ],
      toolbox: {
        orient: "vertical",
        top: '5%',
        left: '1%',
        feature: {
          saveAsImage: {
            title: "Save as Image"
          }
        }
      },
      legend: {
        top: '12.5%'
      }
      /* treshold
      visualMap: {
        top: 20,
        right: 20,
        pieces: [{
          gt: 0,
          lte: 1000,
          color: 'red'
        },{
          gt: 1000,
          lte: 3000,
          color: 'blue'
        }]}*/
    };
  }

  /**
   * Update graph with dataset.
   */
  private updateGraph() {
    this.updateOptions.series = Array.from(this.allData).map(([key, value]) => this.createSeries(key, value));
    this.updateOptions.legend.data = Array.from(this.allData.keys()).map(key => this.dataDictionaryService.translate(key));
  }

  /**
   * Sets day to next or previous based on param.
   * @param next Sets next or previous date.
   */
  public nextDay(next:boolean){
    this.dataDate.setDate(this.dataDate.getDate() + (next ? 1 : -1));
    this.dateInput.nativeElement.valueAsDate = this.dataDate;
    this.setDateProcess(this.dataDate);
  }

  /**
   * Returns next possible sensor "id".
   * @param next 
   * @returns next/prev sensor id.
   */
  public getSensor(next:boolean){
    let sensorName = parseInt(this.sensorName,0)

    return sensorName + (next ? 1 : -1);
  }

  /**
   * Can load any next sensor from dictionary.
   * @returns boolean true if yes.
   */
  public canChangeToPrev(){
    return parseInt(this.sensorName,0) > 0
  }

  /**
   * Can load any prev sensor from dictionary.
   * @returns boolean true if yes.
   */
  public canChangeToNext(){
    let current = this.dataDictionaryService.translate(this.sensorName);
    if(this.sensorName === current){
      return false;
    }
    let nextId = String(parseInt(this.sensorName,0)+1)
    let nextSensor = this.dataDictionaryService.translate(nextId)

    return nextSensor !== nextId ? true : false
  }

  public isCurrentDate(){
    const tmpDate = new Date();
    return this.dataDate.getFullYear() === tmpDate.getFullYear() &&
    this.dataDate.getMonth() === tmpDate.getMonth() &&
    this.dataDate.getDate() === tmpDate.getDate();
  }
}
