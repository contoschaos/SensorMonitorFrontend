import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SensorTemplateComponent } from './sensor-template.component';

describe('SensorTemplateComponent', () => {
  let component: SensorTemplateComponent;
  let fixture: ComponentFixture<SensorTemplateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SensorTemplateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SensorTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
