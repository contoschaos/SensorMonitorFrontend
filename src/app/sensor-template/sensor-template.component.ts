import { Component, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges } from '@angular/core';
import { InternalUpdateService } from '../internal-update.service';
import { Subscription } from 'rxjs';
import { DataDictionaryService } from '../data-dictionary.service';

@Component({
  selector: 'app-sensor-template',
  templateUrl: './sensor-template.component.html',
  styleUrls: ['./sensor-template.component.sass']
})
export class SensorTemplateComponent implements OnInit, OnDestroy {
  /**Name of sensor. */
  @Input()
  sensorName: string | undefined;
  /**Data shown in view.*/
  attributes: Map<string, string> = new Map();
  /**Alert level*/
  alertLvl: string | undefined;
  /**Alert message*/
  alertMsg: string | undefined;
  /**Listener for changes. */
  private subscribtion: Subscription | undefined;

  constructor(private dataDictionaryService: DataDictionaryService, private internalUpdate: InternalUpdateService) { }

  ngOnInit(): void {
    if (this.sensorName) {
      this.setSensor(this.internalUpdate.getCurrentData(this.sensorName));
    }

    this.subscribtion = this.internalUpdate.sensorData.subscribe((updateData: any) => {
      if (Object.keys(updateData)[0] == this.sensorName) {
        this.setSensor(updateData);
      }
    });
  }

  ngOnDestroy(): void {
    if (this.subscribtion) {
      this.subscribtion.unsubscribe();
    }
  }

  getSensorName() {
    if (this.sensorName) {
      return this.dataDictionaryService.translateReverse(this.sensorName);
    }
  }

  getClass() {
    switch (this.alertLvl) {
      case '1': return "war"
      case '2': return "alert"
      default: return "normal"
    }
  }

  /**
   * Update attributes of sensor.
   * @param data Data from sensor
   */
  private setSensor(data: any) {
    if (this.sensorName == undefined) {
      return;
    }
    let attributes = data[this.sensorName]
    Object.keys(attributes).forEach(attName => {
      this.attributes.set(this.dataDictionaryService.translate(attName), attributes[attName])
    })

    if (Object.keys(data).length > 1) {
      this.alertLvl = data.lvl
      this.alertMsg = data.msg
    }
  }

}
