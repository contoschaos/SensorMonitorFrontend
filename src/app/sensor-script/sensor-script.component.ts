import { Component, OnInit } from '@angular/core';
import { ChildActivationStart, Router } from '@angular/router';
import { RestService } from '../rest.service';

const NEW_KEY:string = "New"

@Component({
  selector: 'app-sensor-script',
  templateUrl: './sensor-script.component.html',
  styleUrls: ['./sensor-script.component.sass']
})
export class SensorScriptComponent implements OnInit {
  sensors:string[]=[]
  selected:string = ''
  script:string   = ''
  result:string = ''
  dictionarySensor:[]=[]
  dictionaryAtt:[]=[]

  constructor(private route : Router,private rest:RestService) { }

  ngOnInit(): void {
    this.rest.getScripts().subscribe((rawList)=>{
      let list:string[] = rawList
      list.unshift(NEW_KEY)
      this.sensors = list
    })
    this.rest.getDictionary().subscribe((rawDictionary)=>{
      console.log(rawDictionary)
      this.dictionarySensor = JSON.parse(rawDictionary).sensors
      this.dictionaryAtt = JSON.parse(rawDictionary).attributes
    })
  }

  createNewSensor(){
    //call create python file service call
  }

  showText(event:any){
    if(this.selected!=NEW_KEY){
      this.rest.getScript(this.selected).subscribe(rawScript=>{
        this.script = JSON.parse(rawScript)

      })
    }else{
      this.script = ''
    }
  }

  runTest(){
    if(this.selected == NEW_KEY || this.selected == ''){
      return
    }

    this.rest.getTestRun(this.selected).subscribe(
      rawData=>this.result = rawData
      )
  }

  //TODO return html to be mad as row
  getTableRow(object:any){
    let key = Object.keys(object)[0]
    let value = object[key]

    return `${key} : ${value}`
  }

}
