import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SensorScriptComponent } from './sensor-script.component';

describe('SensorScriptComponent', () => {
  let component: SensorScriptComponent;
  let fixture: ComponentFixture<SensorScriptComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SensorScriptComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SensorScriptComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
