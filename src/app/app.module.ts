import { LOCALE_ID, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SensorScriptComponent } from './sensor-script/sensor-script.component';
import { SensorTemplateComponent } from './sensor-template/sensor-template.component';
import { OverviewComponent } from './overview/overview.component';
import { HistoryComponent } from './history/history.component';

import { FormsModule } from '@angular/forms';
import { NgxEchartsModule } from 'ngx-echarts';
import { LoginComponent } from './login/login.component';
import { RestService } from './rest.service';
import { InternalUpdateService } from './internal-update.service';
import { PrismComponent } from './prism/prism.component';
import 'prismjs/components/prism-python';
import 'prismjs/components/prism-json';
// Add more prism language if need!

//create routes for root component
const appRoutes: Routes =[
  {path: '',            component:OverviewComponent},
  {path: 'login',       component:LoginComponent},
  {path: 'sensor-script',  component:SensorScriptComponent},
  {path: 'history',     component:HistoryComponent}
];

@NgModule({ declarations: [
        AppComponent,
        SensorScriptComponent,
        SensorTemplateComponent,
        OverviewComponent,
        HistoryComponent,
        LoginComponent,
        PrismComponent
    ],
    bootstrap: [AppComponent],
    exports: [
        PrismComponent
    ], imports: [AppRoutingModule,
        BrowserModule,
        FormsModule,
        RouterModule.forRoot(appRoutes), //add all routes from rootComponent
        NgxEchartsModule.forRoot({ echarts: () => import('echarts') })], providers: [RestService, InternalUpdateService, provideHttpClient(withInterceptorsFromDi())] })
export class AppModule { 
}

