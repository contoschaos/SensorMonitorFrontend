import { HttpClient } from '@angular/common/http';
import { Injectable, Optional, SkipSelf } from '@angular/core';
import { throwError } from 'rxjs';
import { catchError, retry } from 'rxjs';

//let NOT_SPECIFIED = 'Device IP not specified' 

@Injectable({
  providedIn: 'root'
})
export class RestService {
  endpoint: string = '192.168.0.121'

  processError(err: any) {
    let message = '';
    if (err.error instanceof ErrorEvent) {
      message = err.error.message;
    } else {
      message = `Error Code: ${err.status}\nMessage: ${err.message}`;
    }
    console.error(message);
    return throwError(() => new Error(message));
  }

  constructor(private http: HttpClient, @Optional() @SkipSelf() private self?: RestService) {
    if (self) {
      throw new Error("Service: RestService is not singleton.")
    }
  }

  public getCurrent() {
    const specificEnd = `http://${this.endpoint}:8752/current`;
    return this.http.get<any>(specificEnd).pipe(retry(1), catchError(this.processError));
  }

  public getHistory(data: Date) {
    const opt = [{ day: '2-digit' }, { month: '2-digit' }, { year: 'numeric' }];
    const formatedDate = this.formatDate(data, opt, '-');
    const specificEnd = `http://${this.endpoint}:8752/history/${formatedDate}`;
    return this.http.get<any>(specificEnd).pipe(retry(1), catchError(this.processError));
  }

  public getDictionary() {
    const specificEnd = `http://${this.endpoint}:8752/dictionary`;
    return this.http.get<any>(specificEnd).pipe(retry(1), catchError(this.processError));
  }

  public getScripts() {
    const specificEnd = `http://${this.endpoint}:8752/getScripts`;
    return this.http.get<any>(specificEnd).pipe(retry(1), catchError(this.processError));
  }
  public getScript(scriptName: string) {
    const specificEnd = `http://${this.endpoint}:8752/getScript/${scriptName}`;
    return this.http.get<any>(specificEnd).pipe(retry(1), catchError(this.processError));
  }

  public getTestRun(scriptName: string) {
    const specificEnd = `http://${this.endpoint}:8752/testRun/${scriptName}`;
    return this.http.get<any>(specificEnd).pipe(retry(1), catchError(this.processError));
  }

  public setEndpoint(ip: string) {
    this.endpoint = ip;
  }

  public getEndpoint() {
    return this.endpoint;
  }

  private formatDate(date: Date, parts: any, joinString: string) {
    return parts.map(
      (opt: any) => {
        let f = new Intl.DateTimeFormat('en', opt);
        return f.format(date);
      }).join(joinString);
  }
}
