import { Injectable, Optional, SkipSelf } from '@angular/core';
import { Subject } from 'rxjs';
import { DataDictionaryService } from './data-dictionary.service';

@Injectable({
  providedIn: 'root'
})
export class InternalUpdateService {
  private sensorDataSource = new Subject<any[]>();
  sensorData = this.sensorDataSource.asObservable();

  /**All current sensor data */
  private sensorMap = new Map();

  constructor(private dataDictionaryService: DataDictionaryService, @Optional() @SkipSelf() private self?: InternalUpdateService) {
    if (self) {
      throw new Error("Service: InternalUpdateService is not singleton.")
    }
  }

  /**
   * Update sensor data.
   * @param pSensorData Data to be updated.
   */
  pushSensorData(pSensorData: any) {
    let sensorName = Object.keys(pSensorData)[0];

    this.sensorMap.set(sensorName, pSensorData);

    this.sensorDataSource.next(pSensorData);
  }

  /**
   * Get latest data for sensor.
   * @param pName Name of sensor.
   * @returns Current data for sensor.
   */
  getCurrentData(pName: string) {
    return this.sensorMap.get(pName);
  }
}
