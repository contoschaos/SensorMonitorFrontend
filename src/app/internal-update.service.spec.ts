import { TestBed } from '@angular/core/testing';

import { InternalUpdateService } from './internal-update.service';

describe('InternalUpdateService', () => {
  let service: InternalUpdateService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(InternalUpdateService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
